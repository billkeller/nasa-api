// NASA Mars photos API: https://api.nasa.gov/api.html#MarsPhotos
var showRoverPhotos = function(dateSelected, camera){
	$.ajax({
		type: 'GET',
		url: 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?camera=' + camera + '&earth_date=' + dateSelected + '&api_key=67YQKnEkJX3deRla73qWfKtolR90WuqO2TXh8Jgf'
	}).success(function(data){
		console.log('data: ', data);
		var photoArr = data.photos;
		// console.log(photoArr);

		for(var i = 0; i < photoArr.length; i++){
			console.log(photoArr[i].img_src);
			$('#photos').append('<img src="' + photoArr[i].img_src + '">');
		}
		
	}).error(function(error){
		console.log('error msg: ', error);
	});
};

$(document).on('ready', function(){
	

	$('form').on('submit', function(e){
		e.preventDefault();
		$('#photos').empty();
		var dateSelected = $('#date-selected').val();
		var camera = $('input[name="camera"]:checked').val();
		showRoverPhotos(dateSelected, camera);
	});


});


